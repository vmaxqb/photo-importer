# Photo Importer

Imports photos from phone to some place, putting them in date-related folders

## Getting started

Copy `config.ini.example` to `config.ini` and specify `DIR_FROM` and `DIR_TO` in it.
Then run `php photo-import.php`.

## License
Licence is MIT
