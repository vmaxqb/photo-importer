<?php
    function show_status($done, $total, $msg, $size = 30) {
        static $start_time;

        if ($done > $total)
            return;

        if (empty($start_time))
            $start_time = time();
        $now = time();

        $perc = $done / $total;
        $bar = floor($perc * $size);

        $status_bar = '[' . str_repeat('=', $bar);
        if ($bar < $size) {
            $status_bar .= '>' . str_repeat(' ', $size - $bar);
        } else {
            $status_bar .= '=';
        }
        $status_bar .= '] ' . floor($perc * 100) . '%  ' . $done . '/' . $total;

        $elapsed = $now - $start_time;
        $eta = round($elapsed / $done * ($total - $done), 2);

        $status_bar .= ' remaining: ' . number_format($eta) . ' sec.  elapsed: ' . number_format($elapsed) . ' sec.';

        system('clear');
        echo $msg . "\n" . $status_bar . "\n";
        flush();
    }

    function recursiveScan(string $dir): ?array
    {
        $result = [];
        $files = scandir($dir);
        if (empty($files)) {
            return null;
        }
        foreach ($files as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }
            $fullFile = $dir . DIRECTORY_SEPARATOR . $file;
            if (is_file($fullFile) && preg_match("/\.(heic|jpg|png|mov|mp4)$/i", $file)) {
                $result[] = $fullFile;
            } elseif (is_dir($fullFile)) {
                $subDirResult = recursiveScan($fullFile);
                if (!empty($subDirResult)) {
                    $result = array_merge($result, $subDirResult);
                }
            }
        }
        return $result;
    }

    $ini = parse_ini_file('config.ini');
    $dirFrom = $ini['DIR_FROM'] ?? '';
    $dirTo = $ini['DIR_TO'] ?? '';
    $dirFrom = rtrim($dirFrom, DIRECTORY_SEPARATOR);
    $dirTo = rtrim($dirTo, DIRECTORY_SEPARATOR);
    if (empty($dirFrom) || empty($dirTo)) {
        echo ("DIR_FROM and DIR_TO must be specified in config.ini\n");
        die;
    }

    $fileList = recursiveScan($dirFrom);
    $fileListCount = count($fileList);
    $i = 0;
    foreach ($fileList as $file) {
        $i++;
        $ctime = filemtime($file) ?: filectime($file);
        $dstDir = $dirTo . DIRECTORY_SEPARATOR . date('Y', $ctime) . ' год' . DIRECTORY_SEPARATOR . date('Y-m-d', $ctime);
        $dstFile = $dstDir . DIRECTORY_SEPARATOR . basename($file);
        show_status($i, $fileListCount, 'Copying from: ' . $file . "\nCopying to  : " . $dstFile);
        if (!is_dir($dstDir) && !@mkdir($dstDir, 0777, true)) {
            continue;
        }
        if (!copy($file, $dstFile)) {
            continue;
        }
        unlink($file);
    }

    echo "Done\n";
